package com.crypto.cash.crashsafe;

import android.content.Context;

public class ExceptionHandler implements Thread.UncaughtExceptionHandler {

    public Context context;

    public ExceptionHandler(Context context) {
        this.context = context;
    }

    public synchronized void uncaughtException(Thread thread, Throwable exception) {
        exception.printStackTrace();
        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(10);
    }

}
