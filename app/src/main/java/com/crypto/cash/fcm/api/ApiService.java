//package com.crypto.cash.fcm.api;
//
//import java.util.List;
//
//import retrofit2.http.Field;
//import retrofit2.http.FormUrlEncoded;
//import retrofit2.http.POST;
//import ro.android.ecotrans.structure.models.LoginResponseModel;
//import ro.android.ecotrans.structure.models.NotificationModel;
//import ro.android.ecotrans.structure.models.OrderModel;
//import ro.android.ecotrans.structure.models.SubscriptionModel;
//import ro.android.ecotrans.structure.models.SuccessResponseModel;
//import ro.android.ecotrans.structure.models.SuccessUpdateTokenModel;
//import rx.Observable;
//
//public interface ApiService {
//
//    //------------------------------------------------------------------------------------------------------------------------
//    // LoginResponseModel
//    @POST("auth")
//    @FormUrlEncoded
//    Observable<LoginResponseModel> login(@Field("email") String email,
//                                         @Field("password") String password);
//
//    @POST("register")
//    @FormUrlEncoded
//    Observable<LoginResponseModel> register(@Field("password") String password,
//                                            @Field("first_name") String firstName,
//                                            @Field("last_name") String lastName,
//                                            @Field("languages") String languages,
//                                            @Field("profession") String profession,
//                                            @Field("phone") String phone,
//                                            @Field("email") String email,
//                                            @Field("company_cif") String companyCif,
//                                            @Field("company_name") String companyName,
//                                            @Field("company_country") String companyCountry,
//                                            @Field("company_city") String companyCity,
//                                            @Field("company_zip_code") String companyZipCode,
//                                            @Field("company_county") String companyCounty,
//                                            @Field("company_address") String companyAddress,
//                                            @Field("company_desciption") String companyDescription,
//                                            @Field("company_reg_com") String companyRegCom,
//                                            @Field("company_phone") String companyPhone,
//                                            @Field("gcm_sender_id") String gcmSenderId);
//
//    @POST("checkToken")
//    @FormUrlEncoded
//    Observable<LoginResponseModel> checkFCMToken(@Field("token") String token);
//    //------------------------------------------------------------------------------------------------------------------------
//
//    //------------------------------------------------------------------------------------------------------------------------
//    // SuccessResponseModel
//    @POST("logout")
//    @FormUrlEncoded
//    Observable<SuccessResponseModel> logout(@Field("token") String token);
//
//    @POST("setPosition")
//    @FormUrlEncoded
//    Observable<SuccessResponseModel> setPosition(@Field("token") String token,
//                                                 @Field("lat") double lat,
//                                                 @Field("lng") double lng,
//                                                 @Field("speed") float speed);
//
//    @POST("setStatus")
//    @FormUrlEncoded
//    Observable<SuccessResponseModel> setStatus(@Field("token") String token,
//                                               @Field("status") int status);
//
//    @POST("aquireOrder")
//    @FormUrlEncoded
//    Observable<SuccessResponseModel> acquireOrder(@Field("token") String token,
//                                                  @Field("order_id") String order_id);
//
//    @POST("updateOrderStatus")
//    @FormUrlEncoded
//    Observable<SuccessResponseModel> updateOrderStatus(@Field("token") String token,
//                                                       @Field("order_id") String orderId,
//                                                       @Field("status") String status);
//
//    @POST("addFavorite")
//    @FormUrlEncoded
//    Observable<SuccessResponseModel> addOrderToFav(@Field("token") String token,
//                                                   @Field("order_id") String orderId);
//
//    @POST("removeFavorite")
//    @FormUrlEncoded
//    Observable<SuccessResponseModel> removeOrderFromFav(@Field("token") String token,
//                                                        @Field("order_id") String orderId);
//
//    @POST("updateGcmToken")
//    @FormUrlEncoded
//    Observable<SuccessUpdateTokenModel> updateFCMToken(@Field("token") String token,
//                                                       @Field("gcm_user_id") String gcmSenderId);
//
//    @POST("clearNotifications")
//    @FormUrlEncoded
//    Observable<SuccessResponseModel> clearNotifications(@Field("token") String token);
//
//    @POST("postSubscription")
//    @FormUrlEncoded
//    Observable<SuccessResponseModel> postSubscription(@Field("token") String token,
//                                                      @Field("subscription_id") String subscription_id);
//
//    @POST("edit_pass")
//    @FormUrlEncoded
//    Observable<SuccessResponseModel> editPass(@Field("token") String token,
//                                              @Field("password") String password);
//
//    // TEST
//    @POST("postBatteryInfo")
//    @FormUrlEncoded
//    Observable<SuccessResponseModel> postBatteryInfo(@Field("token") String token,
//                                                     @Field("battery_level") int batteryLevel);
//    //------------------------------------------------------------------------------------------------------------------------
//
//
//    //------------------------------------------------------------------------------------------------------------------------
//    // List<OrderModel
//    @POST("orders")
//    @FormUrlEncoded
//    Observable<List<OrderModel>> getOrders(@Field("token") String token,
//                                           @Field("id_locatie_plecare") String id_locatie_plecare,
//                                           @Field("id_locatie_sosire") String id_locatie_sosire,
//                                           @Field("tara_s_plecare") String tara_s_plecare,
//                                           @Field("judet_plecare") String judet_plecare,
//                                           @Field("localitate_plecare") String localitate_plecare,
//                                           @Field("tara_s_sosire") String tara_s_sosire,
//                                           @Field("judet_sosire") String judet_sosire,
//                                           @Field("localitate_sosire") String localitate_sosire,
//                                           @Field("data_inceput_disponibilitate") String data_inceput_disponibilitate,
//                                           @Field("tip_anunt") String tip_anunt,
//                                           @Field("tone") String tone,
//                                           @Field("volum") String volum,
//                                           @Field("podea") String podea,
//                                           @Field("limit") int limit,
//                                           @Field("offset") int offset);
//
//    @POST("postFreeRideDestination")
//    @FormUrlEncoded
//    Observable<List<OrderModel>> postFreeRideDestination(@Field("token") String token,
//                                                         @Field("id_locatie_plecare") String id_locatie_plecare,
//                                                         @Field("tara_s_plecare") String tara_s_plecare,
//                                                         @Field("judet_plecare") String judet_plecare,
//                                                         @Field("localitate_plecare") String localitate_plecare,
//                                                         @Field("data_inceput_disponibilitate") String data_inceput_disponibilitate,
//                                                         @Field("tip_anunt") String tip_anunt,
//                                                         @Field("tone") String tone,
//                                                         @Field("volum") String volum,
//                                                         @Field("podea") String podea,
//                                                         @Field("radius") String radius,
//                                                         @Field("limit") int limit,
//                                                         @Field("offset") int offset);
//
//    @POST("nearbyOrders")
//    @FormUrlEncoded
//    Observable<List<OrderModel>> getCloseOrders(@Field("token") String token,
//                                                @Field("data_inceput_disponibilitate") String data_inceput_disponibilitate,
//                                                @Field("tip_anunt") String tip_anunt,
//                                                @Field("tone") String tone,
//                                                @Field("volum") String volum,
//                                                @Field("podea") String podea,
//                                                @Field("lat") String lat,
//                                                @Field("lng") String lng,
//                                                @Field("radius") String radius,
//                                                @Field("seenIds") String seenIds);
//
//
//
//    @POST("lastOrders")
//    @FormUrlEncoded
//    Observable<List<OrderModel>> getLastOrders(@Field("token") String token,
//                                               @Field("tip_anunt") String tip_anunt,
//                                               @Field("limit") int limit,
//                                               @Field("offset") int offset,
//                                               @Field("seenIds") String seenIds);
//
//
//
//    @POST("getFavorite")
//    @FormUrlEncoded
//    Observable<List<OrderModel>> getFavorriteOrders(@Field("token") String token);
//
//    //------------------------------------------------------------------------------------------------------------------------
//    // List<SubscriptionModel>
//    @POST("getSubscriptions")
//    @FormUrlEncoded
//    Observable<List<SubscriptionModel>> getSubscriptions(@Field("token") String token);
//    //------------------------------------------------------------------------------------------------------------------------
//
//
//    //------------------------------------------------------------------------------------------------------------------------
//    // List<NotificationModel>
//    @POST("getNotifications")
//    @FormUrlEncoded
//    Observable<List<NotificationModel>> getNotifications(@Field("token") String token,
//                                                         @Field("type") String type,
//                                                         @Field("seenIds") String seenIds);
//    //------------------------------------------------------------------------------------------------------------------------
//
//}
