//package com.crypto.cash.fcm.api;
//
//import com.google.gson.GsonBuilder;
//
//import java.util.concurrent.TimeUnit;
//
//import okhttp3.OkHttpClient;
//import okhttp3.logging.HttpLoggingInterceptor;
//import retrofit2.Retrofit;
//import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
//import retrofit2.converter.gson.GsonConverterFactory;
//import ro.android.ecotrans.BuildConfig;
//import ro.android.ecotrans.fcm.NotificationDeserializer;
//import ro.android.ecotrans.fcm.NotificationSerializer;
//import ro.android.ecotrans.structure.models.NotificationModel;
//
//public class RestClient {
//
//    private static RestClient mRestClient;
//    private static Retrofit retrofit;
//    private ApiService apiService;
//
//    public static RestClient getInstance() {
//        if (mRestClient == null) {
//            mRestClient = new RestClient();
//        }
//        return mRestClient;
//    }
//
//    public ApiService getApiService() {
//        if (apiService == null) {
//            apiService = retrofit.create(ApiService.class);
//        }
//        return apiService;
//    }
//
//    private RestClient() {
//        OkHttpClient.Builder okHttpClientBuilder = new OkHttpClient.Builder();
//        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
//
//        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
//        okHttpClientBuilder.addInterceptor(httpLoggingInterceptor);
//
//        // 60 default second timeout
//        OkHttpClient okHttpClient = okHttpClientBuilder
//                .connectTimeout(60, TimeUnit.SECONDS)
//                .writeTimeout(60, TimeUnit.SECONDS)
//                .readTimeout(60, TimeUnit.SECONDS)
//                .build();
//
//        retrofit = new Retrofit.Builder()
//                .baseUrl(BuildConfig.API_BASE_URL)
//                .client(okHttpClient)
//                .addConverterFactory(
//                        GsonConverterFactory.create(
//                            new GsonBuilder()
//                                .registerTypeAdapter(NotificationModel.class, new NotificationSerializer())
//                                .registerTypeAdapter(NotificationModel.class, new NotificationDeserializer())
//                                .excludeFieldsWithoutExposeAnnotation() // every field used by GSON must have the @Expose annotation
//                                .serializeNulls()
//                                .setDateFormat("yyyy-MM-dd HH:mm:ss")
//                                .setPrettyPrinting()
//                                .create()))
//                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
//                .build();
//    }
//}
