package com.crypto.cash.utils.network;

/**
 * Created by paulsolea on 10/31/16.
 */
public interface ReachabilityInterface {
    void onReachabilityTestPassed();

    void onReachabilityTestFailed();
}
