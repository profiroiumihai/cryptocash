package com.crypto.cash.utils.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;

/**
 * Created by paulsolea on 10/31/16.
 */
public class NetworkUtil {

    public static boolean HAS_CDN = false;
    public static boolean WIFI_CONNECTED = false;
    public static boolean MOBILE_DATA_CONNECTED = false;
    public static boolean MOBILE_DATA_TETHER_CONNECTED = false;
    public static boolean DUMMY_CONNECTED = false;
    public static boolean ETHERNET_CONNECTED = false;
    public static boolean WIMAX_CONNECTED = false;
    public static boolean VPN_CONNECTED = false;
    public static boolean BT_CONNECTED = false;

    public static boolean HAS_INTERNET = false;

    public Context context;

    public NetworkUtil(Context context) {
        this.context = context;
//        HAS_CDN = false;
//        WIFI_CONNECTED = false;
//        MOBILE_DATA_CONNECTED = false;
//        MOBILE_DATA_TETHER_CONNECTED = false;
//        DUMMY_CONNECTED = false;
//        ETHERNET_CONNECTED = false;
//        WIMAX_CONNECTED = false;
//        VPN_CONNECTED = false;
//        BT_CONNECTED = false;
    }

//    public static boolean hasActiveInternet(Context context) {
//        Reachability r = new Reachability(context, "google.com", 80);
//        try {
//            return r.execute().get().booleanValue();
//        } catch (Exception e) {
//            return false;
//        }
//    }

    public boolean hasActiveInternet() {
        new Reachability(context, "google.com", 80, new ReachabilityInterface() {
            @Override
            public void onReachabilityTestPassed() {
                HAS_INTERNET = true;
            }

            @Override
            public void onReachabilityTestFailed() {
                HAS_INTERNET = false;
            }
        }).execute();
        return HAS_INTERNET;
    }

//    public static boolean hasCdn(Context context) {
//        Reachability r = new Reachability(context, "ecotrans.ro", 80);
//        try {
//            HAS_CDN = r.execute().get().booleanValue();
//        } catch (Exception e) {
//            return false;
//        }
//        return HAS_CDN;
//    }

    public boolean hasCdn() {
        new Reachability(context, "ecotrans.ro", 80, new ReachabilityInterface() {
            @Override
            public void onReachabilityTestPassed() {
                HAS_CDN = true;
            }

            @Override
            public void onReachabilityTestFailed() {
                HAS_CDN = false;
            }
        }).execute();
        return HAS_CDN;
    }

    public boolean isConnectionFast(int type, int subType) {
        if (type == ConnectivityManager.TYPE_WIFI) {
            return true;
        } else if (type == ConnectivityManager.TYPE_MOBILE) {
            switch (subType) {
                case TelephonyManager.NETWORK_TYPE_1xRTT:
                    return false; // ~ 50-100 kbps
                case TelephonyManager.NETWORK_TYPE_CDMA:
                    return false; // ~ 14-64 kbps
                case TelephonyManager.NETWORK_TYPE_EDGE:
                    return false; // ~ 50-100 kbps
                case TelephonyManager.NETWORK_TYPE_EVDO_0:
                    return true; // ~ 400-1000 kbps
                case TelephonyManager.NETWORK_TYPE_EVDO_A:
                    return true; // ~ 600-1400 kbps
                case TelephonyManager.NETWORK_TYPE_GPRS:
                    return false; // ~ 100 kbps
                case TelephonyManager.NETWORK_TYPE_HSDPA:
                    return true; // ~ 2-14 Mbps
                case TelephonyManager.NETWORK_TYPE_HSPA:
                    return true; // ~ 700-1700 kbps
                case TelephonyManager.NETWORK_TYPE_HSUPA:
                    return true; // ~ 1-23 Mbps
                case TelephonyManager.NETWORK_TYPE_UMTS:
                    return true; // ~ 400-7000 kbps

                /*
                 * Above API level 7, make sure to set android:targetSdkVersion
                 * to appropriate level to use these
                 */

                case TelephonyManager.NETWORK_TYPE_EHRPD: // API level 11
                    return true; // ~ 1-2 Mbps
                case TelephonyManager.NETWORK_TYPE_EVDO_B: // API level 9
                    return true; // ~ 5 Mbps
                case TelephonyManager.NETWORK_TYPE_HSPAP: // API level 13
                    return true; // ~ 10-20 Mbps
                case TelephonyManager.NETWORK_TYPE_IDEN: // API level 8
                    return false; // ~25 kbps
                case TelephonyManager.NETWORK_TYPE_LTE: // API level 11
                    return true; // ~ 10+ Mbps
                // Unknown
                case TelephonyManager.NETWORK_TYPE_UNKNOWN:
                default:
                    return false;
            }
        } else {
            return false;
        }
    }

    public NetworkInfo getNetworkInfo(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo();
    }

    public String getConnectivityStatus(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        if (activeNetwork != null && activeNetwork.isConnected()) {

            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                return "wifi_connection";
            } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                return "mobile_data_connection";
            } else if (activeNetwork.getType() == ConnectivityManager.TYPE_ETHERNET) {
                return "ethernet_connection";
            } else if (activeNetwork.getType() == ConnectivityManager.TYPE_WIMAX) {
                return "wimax_connection";
            } else if (activeNetwork.getType() == ConnectivityManager.TYPE_DUMMY) {
                return "dummy_connection";
            } else if (activeNetwork.getType() == ConnectivityManager.TYPE_VPN) {
                return "vpn_connection";
            } else if (activeNetwork.getType() == ConnectivityManager.TYPE_BLUETOOTH) {
                return "bluetooth_tether_connection";
            } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE_DUN) {
                return "mobile_data_tether_connection";
            }
        }
        return "no_connection";
    }

    public void getConnectivityStatusString(Context context) {

        switch (new NetworkUtil(context).getConnectivityStatus(context)) {

            case "wifi_connection":
                WIFI_CONNECTED = true;
                break;

            case "mobile_data_connection":
                MOBILE_DATA_CONNECTED = true;
                break;

            case "ethernet_connection":
                ETHERNET_CONNECTED = true;
                break;

            case "wimax_connection":
                WIMAX_CONNECTED = true;
                break;

            case "dummy_connection":
                DUMMY_CONNECTED = true;
                break;

            case "vpn_connection":
                VPN_CONNECTED = true;
                break;

            case "bluetooth_tether_connection":
                BT_CONNECTED = true;
                break;

            case "mobile_data_tether_connection":
                MOBILE_DATA_TETHER_CONNECTED = true;
                break;

            case "no_connection":
                HAS_CDN = false;
                WIFI_CONNECTED = false;
                MOBILE_DATA_CONNECTED = false;
                MOBILE_DATA_TETHER_CONNECTED = false;
                DUMMY_CONNECTED = false;
                ETHERNET_CONNECTED = false;
                WIMAX_CONNECTED = false;
                VPN_CONNECTED = false;
                BT_CONNECTED = false;
                break;

            default:
                HAS_CDN = false;
                WIFI_CONNECTED = false;
                MOBILE_DATA_CONNECTED = false;
                MOBILE_DATA_TETHER_CONNECTED = false;
                DUMMY_CONNECTED = false;
                ETHERNET_CONNECTED = false;
                WIMAX_CONNECTED = false;
                VPN_CONNECTED = false;
                BT_CONNECTED = false;
                break;
        }

        /*
        if (conn.equals("wifi_connection")) {
        	WIFI_CONNECTED = true;
        } else if (conn.equals("mobile_data_connection")) {
            MOBILE_DATA_CONNECTED = true;
        } else if (conn.equals("ethernet_connection")) {
            ETHERNET_CONNECTED = true;
        } else if (conn.equals("wimax_connection")) {
            WIMAX_CONNECTED = true;
        } else if (conn.equals("dummy_connection")) {
            DUMMY_CONNECTED = true;
        } else if (conn.equals("vpn_connection")) {
            VPN_CONNECTED = true;
        } else if (conn.equals("bluetooth_tether_connection")) {
            BT_CONNECTED = true;
        } else if (conn.equals("mobile_data_tether_connection")) {
            MOBILE_DATA_TETHER_CONNECTED = true;
        } else if (conn.equals("no_connection")) {
            HAS_CDN = false;
            WIFI_CONNECTED = false;
            MOBILE_DATA_CONNECTED = false;
            MOBILE_DATA_TETHER_CONNECTED = false;
            DUMMY_CONNECTED = false;
            ETHERNET_CONNECTED = false;
            WIMAX_CONNECTED = false;
            VPN_CONNECTED = false;
            BT_CONNECTED = false;
        }
        */
    }

    public boolean hasWifiInternet(Context context) {
        getConnectivityStatusString(context);
        return (WIFI_CONNECTED && hasActiveInternet());
    }

    public boolean hasWifi(Context context) {
        getConnectivityStatusString(context);
        return WIFI_CONNECTED;
    }

    public boolean hasMobileDataInternet(Context context) {
        getConnectivityStatusString(context);
        return (MOBILE_DATA_CONNECTED && hasActiveInternet());
    }

    public boolean hasMobileData(Context context) {
        getConnectivityStatusString(context);
        return MOBILE_DATA_CONNECTED;
    }

    public boolean hasHighSpeedMobileDataInternet(Context context) {
        getConnectivityStatusString(context);
        return (MOBILE_DATA_CONNECTED && hasActiveInternet() && isConnectedFast(context));
    }

    public boolean hasHighSpeedMobileData(Context context) {
        getConnectivityStatusString(context);
        return (MOBILE_DATA_CONNECTED && isConnectedFast(context));
    }

    public boolean hasEthernetInternet(Context context) {
        getConnectivityStatusString(context);
        return (ETHERNET_CONNECTED && hasActiveInternet());
    }

    public boolean hasEthernet(Context context) {
        getConnectivityStatusString(context);
        return ETHERNET_CONNECTED;
    }

    public boolean hasWimaxInternet(Context context) {
        getConnectivityStatusString(context);
        return (WIMAX_CONNECTED && hasActiveInternet());
    }

    public boolean hasWimax(Context context) {
        getConnectivityStatusString(context);
        return WIMAX_CONNECTED;
    }

    public boolean hasMobileDataTetherInternet(Context context) {
        getConnectivityStatusString(context);
        return (MOBILE_DATA_TETHER_CONNECTED && hasActiveInternet());
    }

    public boolean hasMobileDataTether(Context context) {
        getConnectivityStatusString(context);
        return MOBILE_DATA_TETHER_CONNECTED;
    }

    public boolean hasDummyInternet(Context context) {
        getConnectivityStatusString(context);
        return (DUMMY_CONNECTED && hasActiveInternet());
    }

    public boolean hasDummy(Context context) {
        getConnectivityStatusString(context);
        return DUMMY_CONNECTED;
    }

    public boolean hasVpnInternet(Context context) {
        getConnectivityStatusString(context);
        return (VPN_CONNECTED && hasActiveInternet());
    }

    public boolean hasVpn(Context context) {
        getConnectivityStatusString(context);
        return VPN_CONNECTED;
    }

    public boolean hasBtInternet(Context context) {
        getConnectivityStatusString(context);
        return (BT_CONNECTED && hasActiveInternet());
    }

    public boolean hasBt(Context context) {
        getConnectivityStatusString(context);
        return BT_CONNECTED;
    }

    public boolean isConnectedFastInternet(Context context) {
        NetworkInfo info = getNetworkInfo(context);
        return (info != null && hasActiveInternet() && info.isConnected() && isConnectionFast(info.getType(), info.getSubtype()));
    }

    public boolean isConnectedFast(Context context) {
        NetworkInfo info = getNetworkInfo(context);
        return (info != null && info.isConnected() && isConnectionFast(info.getType(), info.getSubtype()));
    }
}
