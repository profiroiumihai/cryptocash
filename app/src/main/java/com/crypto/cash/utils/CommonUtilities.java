//package com.crypto.cash.utils;
//
//import android.app.Activity;
//import android.app.AlertDialog;
//import android.content.Context;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.content.res.Resources;
//import android.net.Uri;
//import android.os.Bundle;
//import android.support.design.widget.BottomSheetDialogFragment;
//import android.support.design.widget.FloatingActionButton;
//import android.support.design.widget.Snackbar;
//import android.support.v4.app.Fragment;
//import android.support.v7.app.AppCompatActivity;
//import android.text.Html;
//import android.text.Spanned;
//import android.text.TextUtils;
//import android.util.Log;
//import android.util.Patterns;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.inputmethod.InputMethodManager;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//
//import com.arlib.floatingsearchview.FloatingSearchView;
//import com.google.android.gms.common.ConnectionResult;
//import com.google.android.gms.common.GooglePlayServicesUtil;
//
//import java.text.SimpleDateFormat;
//import java.util.Calendar;
//import java.util.Date;
//import java.util.Locale;
//import java.util.TimeZone;
//
//import ro.android.ecotrans.Constants;
//import ro.android.ecotrans.R;
//import ro.android.ecotrans.location.LocationUtil;
//import ro.android.ecotrans.structure.models.OrderModel;
//
//import static ro.android.ecotrans.utils.DeviceUtils.getDisplayGoodHeightSize;
//import static ro.android.ecotrans.utils.DeviceUtils.getDisplaySize;
//
//public class CommonUtilities {
//
//    public static String TAG = CommonUtilities.class.getCanonicalName();
//
//    // -2 INDEFINITE -1 SHORT 0 LONG
//    public static void showGPSSnackbar(final Activity activity, View view, int snackbarLength) {
//
//        if (!LocationUtil.isGPSEnabled(activity.getApplicationContext())) {
//            LocationUtil.requireGPSDialog(activity);
//        } else {
//            final Snackbar gpsSnack = Snackbar.make(view, activity.getResources().getString(R.string.status_gps_request_canceled), snackbarLength);
//            gpsSnack.setAction(R.string.status_gps_activate_snackbar, new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    LocationUtil.requireGPSDialog(activity);
//                }
//            });
//            gpsSnack.addCallback(new Snackbar.Callback() {
//                @Override
//                public void onDismissed(Snackbar snackbar, int event) {
//                }
//                @Override
//                public void onShown(Snackbar snackbar) {
//                }
//            });
//            gpsSnack.setDuration(Snackbar.LENGTH_INDEFINITE);
//            gpsSnack.show();
//        }
//    }
//
//    // -2 INDEFINITE -1 SHORT 0 LONG
//    public static void showGMSSnackbar(final Activity activity, View view, int snackbarLength) {
//        final Snackbar gpsSnack = Snackbar.make(view, activity.getResources().getString(R.string.status_gms_request_canceled), snackbarLength);
//        gpsSnack.setAction(R.string.status_gms_retry_snackbar, new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                // Check status of Google Play Services
//                int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(activity);
//                if (resultCode != ConnectionResult.SUCCESS) {
//                    // show your own AlertDialog for example:
//                    AlertDialog.Builder builder = new AlertDialog.Builder(activity);
//                    // set the message
//                    builder.setMessage(activity.getResources().getString(R.string.status_gms_request_canceled))
//                            .setTitle(activity.getResources().getString(R.string.status_gms_request_dialog_title)); // set a title
//
//                    builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
//                        public void onClick(DialogInterface dialog, int id) {
//                            // User clicked OK button
//                            final String appPackageName = "com.google.android.gms";
//                            try {
//                                activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
//                            } catch (android.content.ActivityNotFoundException anfe) {
//                                activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
//                            }
//                        }
//                    });
//                    builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
//                        public void onClick(DialogInterface dialog, int id) {
//                            dialog.dismiss();
//                        }
//                    });
//                    AlertDialog dialog = builder.create();
//                    dialog.show();
//                }
//            }
//        });
//        gpsSnack.addCallback(new Snackbar.Callback() {
//            @Override
//            public void onDismissed(Snackbar snackbar, int event) {
//            }
//            @Override
//            public void onShown(Snackbar snackbar) {
//            }
//        });
//        gpsSnack.show();
//    }
//
//    public static void changeFragment(Context context, Class<? extends Fragment> fragment, String backStack) {
//        try {
//            ((AppCompatActivity) context)
//                    .getSupportFragmentManager()
//                    .beginTransaction()
//                    .replace(R.id.content_frame, fragment.newInstance())
//                    .addToBackStack(backStack)
//                    .commit();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    public static void changeFragmentWithInstance(Context context, Fragment fragment, String backStack) {
//        try {
//            ((AppCompatActivity) context)
//                    .getSupportFragmentManager()
//                    .beginTransaction()
//                    .replace(R.id.content_frame, fragment)
//                    .addToBackStack(backStack)
//                    .commit();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    public static void addFragment(Context context, Class<? extends Fragment> fragment, String tag) {
//        try {
//            ((AppCompatActivity) context)
//                    .getSupportFragmentManager()
//                    .beginTransaction()
//                    .setCustomAnimations(R.anim.slide_from_top, R.anim.exit_to_top)
//                    .add(R.id.content_frame, fragment.newInstance(), tag)
//                    .addToBackStack(null)
//                    .commit();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    public static void addFragment(Context context, Class<? extends Fragment> fragment, Bundle bundle, String tag) {
//        try {
//            Fragment newFragment = fragment.newInstance();
//            newFragment.setArguments(bundle);
//            ((AppCompatActivity) context)
//                    .getSupportFragmentManager()
//                    .beginTransaction()
//                    .setCustomAnimations(R.anim.slide_from_top, R.anim.exit_to_top)
//                    .add(R.id.content_frame, newFragment, tag)
//                    .addToBackStack(null)
//                    .commit();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    public static void removeFragment(Context context, String tag) {
//        Fragment fragment = ((AppCompatActivity) context).getSupportFragmentManager().findFragmentByTag(tag);
//        if (fragment != null) {
//            ((AppCompatActivity) context)
//                    .getSupportFragmentManager()
//                    .beginTransaction()
//                    .setCustomAnimations(R.anim.enter_from_top, R.anim.exit_to_top)
//                    .remove(fragment)
//                    .commit();
//        }
//    }
//
//    public static void changeFragment(Context context, Class<? extends Fragment> fragment) {
//        try {
//            ((AppCompatActivity) context)
//                    .getSupportFragmentManager()
//                    .beginTransaction()
//                    .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
//                    .replace(R.id.content_frame, fragment.newInstance())
//                    .addToBackStack(null)
//                    .commit();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    public static void changeFragmentWithInstance(Context context, Fragment fragment) {
//        try {
//            ((AppCompatActivity) context)
//                    .getSupportFragmentManager()
//                    .beginTransaction()
//                    .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
//                    .replace(R.id.content_frame, fragment)
//                    .addToBackStack(null)
//                    .commit();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    public static boolean validateEmail(final String email) {
//        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches();
//    }
//
//    public static void showAlertDialog(final Context context, final Class<? extends Activity> activityClass, String posBtnText,  String title, String message, final Runnable positiveButtonAction) {
//        try {
//            if (activityClass.cast(context) != null && !activityClass.cast(context).isFinishing()) {
//                new AlertDialog.Builder(context)
//                        .setTitle(title)
//                        .setMessage(message)
//                        .setCancelable(false)
//                        .setPositiveButton(posBtnText, new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                dialog.dismiss();
//                                activityClass.cast(context).runOnUiThread(positiveButtonAction);
//                            }
//                        })
//                        .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                dialog.dismiss();
//                            }
//                        }).show();
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    public static void setAlert(Context context, LayoutInflater inflater, final Runnable possitiveAction,
//                                final Runnable negativeAction, OrderModel orderModel) {
//        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
//        View dialogView = inflater.inflate(R.layout.alert_take_order, null);
//
//        dialogBuilder.setView(dialogView);
//        final AlertDialog alertDialog = dialogBuilder.create();
//
//        TextView tvCurrentOrder = (TextView) dialogView.findViewById(R.id.tvCurrentOrder);
//        tvCurrentOrder.setText(String.format(context.getString(R.string.popup_order_info),
//                orderModel.getLocatie_plecare_text(), orderModel.getLocatie_sosire_text()));
//
//        LinearLayout llAcceptOrder = (LinearLayout) dialogView.findViewById(R.id.llAcceptOrder);
//        llAcceptOrder.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                alertDialog.dismiss();
//                possitiveAction.run();
//            }
//        });
//
//        LinearLayout llRejectOrder = (LinearLayout) dialogView.findViewById(R.id.llRejectOrder);
//        llRejectOrder.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                alertDialog.dismiss();
//                negativeAction.run();
//            }
//        });
//
//        alertDialog.show();
//
//        alertDialog.getWindow().setLayout(getDisplaySize(context).x, getDisplayGoodHeightSize(context));
//    }
//
//    public static void hideSoftKeyboard(Activity activity) {
//        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
//        if (imm.isAcceptingText()) { // verify if the soft keyboard is open
//            try {
//                imm.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//    }
//
////    public static void showAlertDialog(final Context context, final Class<? extends Activity> activityClass, String title, String message, final Runnable positiveButtonAction, final Runnable negativeButtonAction) {
////        try {
////            if (activityClass.cast(context) != null && !activityClass.cast(context).isFinishing()) {
////                new AlertDialog.Builder(context)
////                        .setTitle(title)
////                        .setMessage(message)
////                        .setCancelable(false)
////                        .setPositiveButton(R.string.retry, new DialogInterface.OnClickListener() {
////                            @Override
////                            public void onClick(DialogInterface dialog, int which) {
////                                dialog.dismiss();
////                                activityClass.cast(context).runOnUiThread(positiveButtonAction);
////                            }
////                        })
////                        .setNegativeButton(R.string.exit, new DialogInterface.OnClickListener() {
////                            @Override
////                            public void onClick(DialogInterface dialog, int which) {
////                                dialog.dismiss();
////                                activityClass.cast(context).runOnUiThread(negativeButtonAction);
////                            }
////                        }).show();
////            }
////        } catch (Exception e) {
////            e.printStackTrace();
////        }
////    }
////    public static void showAlertDialog(final Context context, final Class<? extends Activity> activityClass, String title, String message, final Runnable positiveButtonAction) {
////        try {
////            if (activityClass.cast(context) != null && !activityClass.cast(context).isFinishing()) {
////
////                new AlertDialog.Builder(context)
////                        .setTitle(title)
////                        .setMessage(message)
////                        .setCancelable(false)
////                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
////                            @Override
////                            public void onClick(DialogInterface dialog, int which) {
////                                dialog.dismiss();
////                                activityClass.cast(context).runOnUiThread(positiveButtonAction);
////                            }
////                        }).show();
////            }
////        } catch (Exception e) {
////            e.printStackTrace();
////        }
////    }
//
//    public static String getVehicleTypes(String arrayOfIds) {
//        String[] separatedIds = arrayOfIds.split(",");
//        String returnVehicleTypes = "";
//        for (int i = 0; i < separatedIds.length; i++) {
//            returnVehicleTypes += Constants.getVehicleType(Integer.parseInt(separatedIds[i].trim()));
//            if (i < separatedIds.length - 1) {
//                returnVehicleTypes += ", ";
//            }
//        }
//        return returnVehicleTypes;
//    }
//
//    public static String getDateOfTheArticle(String strDate) {
//        Date date = new Date(Long.parseLong(strDate));
//        long timeInMillis = Long.parseLong(strDate);
//
//        int gmtOffset = TimeZone.getTimeZone("GMT+03:00").getRawOffset();
//        long now = System.currentTimeMillis() + gmtOffset;
//        long different = now - timeInMillis;
//
//        long secondsInMilli = 1000;
//        long minutesInMilli = secondsInMilli * 60;
//        long hoursInMilli = minutesInMilli * 60;
//        long daysInMilli = hoursInMilli * 24;
//
//        long elapsedDays = different / daysInMilli;
//        long elapsedHours = different / hoursInMilli;
//
//        SimpleDateFormat resultSdf;
//        int hour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
//
//        if (elapsedDays > 0 || elapsedHours - hour > 0){
//            resultSdf = new SimpleDateFormat("dd-MM-yyyy", new Locale("ro_RO"));
//        }else{
//            resultSdf = new SimpleDateFormat("HH:mm", new Locale("ro_RO"));
//        }
//        return resultSdf.format(date);
//    }
//
//    public static String getCurrentDay() {
//        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", new Locale("ro_RO"));
//        return sdf.format(new Date());
//    }
//
//    public static Spanned formatPlaceDetails(Resources res, CharSequence name, CharSequence address) {
//        Log.e("", res.getString(R.string.place_details, name, address));
//        return Html.fromHtml(res.getString(R.string.place_details, name, address));
//    }
//
//    public static String getTimePassed(long startDate) {
//        int gmtOffset = TimeZone.getTimeZone("GMT+03:00").getRawOffset();
//        long now = System.currentTimeMillis() + gmtOffset;
//        long different = now - startDate * 1000;
//
//        long secondsInMilli = 1000;
//        long minutesInMilli = secondsInMilli * 60;
//        long hoursInMilli = minutesInMilli * 60;
//        long daysInMilli = hoursInMilli * 24;
//
//        long elapsedDays = different / daysInMilli;
//        different = different % daysInMilli;
//
//        long elapsedHours = different / hoursInMilli;
//        different = different % hoursInMilli;
//
//        long elapsedMinutes = different / minutesInMilli;
//        different = different % minutesInMilli;
//
//        if (elapsedDays > 0) {
//            if (elapsedDays == 1) {
//                return "o zi";
//            }
//            return (elapsedDays + 1) + " zile";
//        } else {
//            if (elapsedHours > 0) {
//                if (elapsedHours == 1) {
//                    return "o ora";
//                }
//                return (elapsedHours + 1) + " ore";
//            } else {
//                if (elapsedMinutes == 1) {
//                    return "un minut";
//                }
//                return elapsedMinutes + " minute";
//            }
//        }
//    }
//
//    public static boolean inArrayString(String needle, String[] haystack) {
//        if (haystack == null) {
//            return false;
//        }
//
//        for (String i : haystack) {
//            if (needle.equals(i)) {
//                return true;
//            }
//        }
//        return false;
//    }
//
//    public static int getStatusBarHeight(Context context) {
//        int result = 0;
//        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
//        if (resourceId > 0) {
//            result = context.getResources().getDimensionPixelSize(resourceId);
//        }
//        return result;
//    }
//}
