//package com.crypto.cash.utils.battery;
//
//import android.content.BroadcastReceiver;
//import android.content.Context;
//import android.content.Intent;
//import android.os.BatteryManager;
//import android.os.Build;
//import android.util.Log;
//
//import java.text.DateFormat;
//
//import ro.android.ecotrans.utils.MonitoringService;
//import ro.android.ecotrans.prefs.MultiSharedPrefs;
//import ro.android.ecotrans.utils.Utils;
//
//public class BatteryReceiver extends BroadcastReceiver {
//
//    public static String TAG = BatteryReceiver.class.getCanonicalName();
//
//    public static int chargePlug = -999;
//    public static boolean acCharge = false;
//    public static boolean usbCharge = false;
//    public static boolean wirelessCharge = false;
//
//    public static int level = -999;
//    public static boolean isPresent = false;
//    public static int health = -999;
//    public static int temperature = -999;
//    public static String technology = "null";
//
//    public static int status = -999;
//    public static boolean statusNotCharging = false;
//    public static boolean statusDischarging = false;
//    public static boolean statusCharging = false;
//    public static boolean statusUnknown = false;
//    public static boolean statusFull = false;
//
//    //public static long BATTERY_CHECK_INTERVAL = 60 * 1000L;
//
//    @Override
//    public void onReceive(Context context, Intent intent) {
//
//        Log.d(TAG, intent.getAction());
//
//        chargePlug = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -999);
//        acCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_AC;
//        usbCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_USB;
//        if (Build.VERSION.SDK_INT >= 17) {
//            wirelessCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_WIRELESS;
//        }
//
//        status = intent.getIntExtra(BatteryManager.EXTRA_STATUS, -999);
//        statusUnknown = status == BatteryManager.BATTERY_STATUS_UNKNOWN;
//        statusNotCharging = status == BatteryManager.BATTERY_STATUS_NOT_CHARGING;
//        statusDischarging = status == BatteryManager.BATTERY_STATUS_DISCHARGING;
//        statusCharging = status == BatteryManager.BATTERY_STATUS_CHARGING;
//        statusFull = status == BatteryManager.BATTERY_STATUS_FULL;
//
//        level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -999);
//        isPresent = intent.getBooleanExtra(BatteryManager.EXTRA_PRESENT, false);
//        health = intent.getIntExtra(BatteryManager.EXTRA_HEALTH, -999);
//        technology = intent.getStringExtra(BatteryManager.EXTRA_TECHNOLOGY);
//        temperature = (intent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE, -999)) / 10;
//
//
//        if (!Utils.isMyServiceRunning(context, MonitoringService.class)) {
//            context.startService(new Intent(context, MonitoringService.class));
//        }
//
//        Log.d(TAG, "LAST TIMESTAMP: " +  DateFormat.getInstance().format(MultiSharedPrefs.getPrefs("battery_info").getLong("battery_timestamp", System.currentTimeMillis())));
//        Log.d(TAG, "LAST CHARGE PLUG: " + MultiSharedPrefs.getPrefs("battery_info").getInt("battery_charging_status", chargePlug) + "");
//        Log.d(TAG, "LAST LEVEL: " + MultiSharedPrefs.getPrefs("battery_info").getInt("battery_level", level) + "");
//        Log.d(TAG, "LAST STATUS: " + MultiSharedPrefs.getPrefs("battery_info").getInt("battery_status", status) + "");
//        Log.d(TAG, "LAST TEMPERATURE: " + MultiSharedPrefs.getPrefs("battery_info").getInt("battery_temperature", temperature) + "");
//
//        Log.d(TAG, "CURRENT TIMESTAMP: " + DateFormat.getInstance().format(System.currentTimeMillis()));
//        Log.d(TAG, "CURRENT CHARGE PLUG: " + BatteryReceiver.chargePlug + "");
//        Log.d(TAG, "CURRENT BATTERY: " + BatteryReceiver.level + "");
//        Log.d(TAG, "CURRENT STATUS: " + BatteryReceiver.status + "");
//        Log.d(TAG, "CURRENT TEMPERATURE: " + BatteryReceiver.temperature + "");
//
//
//        long currentTimestamp = System.currentTimeMillis();
//        long lastTimestamp = MultiSharedPrefs.getPrefs("battery_info").getLong("battery_timestamp", System.currentTimeMillis());
//        long timestampDifference = currentTimestamp - lastTimestamp;
//
//        // Get Battery Info
//        if (BatteryReceiver.status == 2 &&
//                (BatteryReceiver.level -
//                        MultiSharedPrefs.getPrefs("battery_info").getInt("battery_level", BatteryReceiver.level) >= 2)) {
//
//            Log.d(TAG, "Battery charging fast, from " +
//                    MultiSharedPrefs.getPrefs("battery_info").getInt("battery_level", BatteryReceiver.level) + " to " +
//                    BatteryReceiver.level + " in the last " +
//                    (timestampDifference / 1000) +
//                    " seconds");
//
//            MultiSharedPrefs.getPrefs("battery_info").putString("battery_charging_state", "fast_charging");
//
//        } else if (BatteryReceiver.status == 2 &&
//                (BatteryReceiver.level -
//                        MultiSharedPrefs.getPrefs("battery_info").getInt("battery_level", BatteryReceiver.level) < 2)) {
//
//            Log.d(TAG, "Battery charging slow, from " +
//                    MultiSharedPrefs.getPrefs("battery_info").getInt("battery_level", BatteryReceiver.level) + " to " +
//                    BatteryReceiver.level + " in the last " +
//                    (timestampDifference / 1000) +
//                    " seconds");
//
//            MultiSharedPrefs.getPrefs("battery_info").putString("battery_charging_state", "slow_charging");
//
//        }  else if (BatteryReceiver.status == 3 &&
//                (MultiSharedPrefs.getPrefs("battery_info").getInt("battery_level", BatteryReceiver.level) -
//                        BatteryReceiver.level >= 2)) {
//
//            Log.d(TAG, "Battery discharging fast, from " +
//                    MultiSharedPrefs.getPrefs("battery_info").getInt("battery_level", BatteryReceiver.level) + " to " +
//                    BatteryReceiver.level +
//                    " in the last " +
//                    (timestampDifference / 1000) +
//                    " seconds");
//
//            MultiSharedPrefs.getPrefs("battery_info").putString("battery_charging_state", "fast_discharging");
//
//        } else if (BatteryReceiver.status == 3 &&
//                (MultiSharedPrefs.getPrefs("battery_info").getInt("battery_level", BatteryReceiver.level) -
//                        BatteryReceiver.level < 2)) {
//
//            Log.d(TAG, "Battery discharging slow, from " +
//                    MultiSharedPrefs.getPrefs("battery_info").getInt("battery_level", BatteryReceiver.level) + " to " +
//                    BatteryReceiver.level +
//                    " in the last " +
//                    (timestampDifference / 1000) +
//                    " seconds");
//
//            MultiSharedPrefs.getPrefs("battery_info").putString("battery_charging_state", "slow_discharging");
//
//        } else if (BatteryReceiver.status == 4) {
//            Log.d(TAG, "Battery not charging");
//
//        } else if (BatteryReceiver.status == 5) {
//            Log.d(TAG, "Battery fully charged");
//        }
//
//        if (intent.getAction().equals(Intent.ACTION_BATTERY_CHANGED) &&
//                timestampDifference
//                > 6 * 30 * 1000 &&
//                timestampDifference
//                <= 7 * 30 * 1000
//                ) {
//
//                Log.d(TAG, "Recording new battery stats");
//                MultiSharedPrefs.getPrefs("battery_info").putLong("battery_timestamp", System.currentTimeMillis());
//                MultiSharedPrefs.getPrefs("battery_info").putInt("battery_charging_status", chargePlug);
//                MultiSharedPrefs.getPrefs("battery_info").putInt("battery_level", level);
//                MultiSharedPrefs.getPrefs("battery_info").putInt("battery_status", status);
//                MultiSharedPrefs.getPrefs("battery_info").putInt("battery_temperature", temperature);
//            }
//    }
//}
