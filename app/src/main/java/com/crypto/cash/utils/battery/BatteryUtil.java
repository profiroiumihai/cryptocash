//package com.crypto.cash.utils.battery;
//
//import android.util.Log;
//
//import ro.android.ecotrans.fcm.api.RestClient;
//import ro.android.ecotrans.prefs.MultiSharedPrefs;
//import ro.android.ecotrans.structure.models.SuccessResponseModel;
//import rx.Observable;
//import rx.Subscriber;
//import rx.android.schedulers.AndroidSchedulers;
//import rx.schedulers.Schedulers;
//
///**
// * Created by paulsolea on 10/31/16.
// */
//public class BatteryUtil {
//
//    public static String TAG = BatteryUtil.class.getCanonicalName();
//
//    private void sendBatteryInfoToServer() {
//        Observable<SuccessResponseModel> batteryObservable = RestClient.getInstance().getApiService().
//                postBatteryInfo(
//                        MultiSharedPrefs.getAppToken(),
//                        MultiSharedPrefs.getPrefs("battery_info").getInt("battery_level", -999));
//
//        batteryObservable.observeOn(AndroidSchedulers.mainThread())
//                .subscribeOn(Schedulers.newThread())
//                .subscribe(new Subscriber<SuccessResponseModel>() {
//                    @Override
//                    public void onCompleted() {
//                        Log.d(TAG, "onCompleted");
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        Log.d(TAG, "onError");
//
//                    }
//
//                    @Override
//                    public void onNext(SuccessResponseModel success) {
//                        Log.d(TAG, success.getSuccess());
//                    }
//                });
//    }
//
//}
