package com.crypto.cash.utils.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.UnknownHostException;

public class Reachability extends AsyncTask<Void, Void, Boolean> {

    public static String TAG = Reachability.class.getCanonicalName();

    private Context mContext;
    private String mHostname;
    private int mServicePort;
    private ReachabilityInterface reachabilityInterface;

    public Reachability(Context context, String hostname, int port, ReachabilityInterface reachabilityInterface) {
        this.mContext = context.getApplicationContext();
        this.mHostname = hostname;
        this.mServicePort = port;
        this.reachabilityInterface = reachabilityInterface;
    }

    public Reachability(Context context, String hostname, int port) {
        this.mContext = context.getApplicationContext();
        this.mHostname = hostname;
        this.mServicePort = port;
        this.reachabilityInterface = null;
    }

    @Override
    public void onPreExecute() {
        Log.d(TAG, "onPreExecute");
    }

    @Override
    public Boolean doInBackground(Void... args) {
        Log.d(TAG, "doInBackground");

        if (isConnected(mContext)) {
            InetAddress address = isResolvable(mHostname);
            if (address != null) {
                if (canConnect(address, mServicePort)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public void onPostExecute(Boolean result) {
        Log.d(TAG, "onPostExecute");

        if (reachabilityInterface != null) {
            Log.d(TAG, result + "");

            if (result) {
                reachabilityInterface.onReachabilityTestPassed();
            } else {
                reachabilityInterface.onReachabilityTestFailed();
            }
        }
    }

    private boolean isConnected(Context context) {
        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo = manager.getActiveNetworkInfo();

        return networkInfo != null && networkInfo.isConnected();

    }

    private InetAddress isResolvable(String hostname) {
        try {
            return InetAddress.getByName(hostname);
        } catch (UnknownHostException e) {
            return null;
        }
    }

    private boolean canConnect(InetAddress address, int port) {
        Socket socket = new Socket();

        SocketAddress socketAddress = new InetSocketAddress(address, port);

        try {
            socket.connect(socketAddress, 2000);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        if (socket.isConnected()) {
            try {
                socket.close();
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
            return true;
        }
        return false;
    }
}