package com.crypto.cash.utils;//package ro.android.ecotrans.utils;
//
//import android.app.Service;
//import android.content.BroadcastReceiver;
//import android.content.Context;
//import android.content.Intent;
//import android.content.IntentFilter;
//import android.os.IBinder;
//import android.util.Log;
//
//import ro.android.ecotrans.prefs.MultiSharedPrefs;
//import ro.android.ecotrans.utils.battery.BatteryReceiver;
//
///**
// * Created by paulsolea on 10/31/16.
// */
//public class MonitoringService extends Service {
//
//    public static String TAG = MonitoringService.class.getCanonicalName();
//
//    public Context context = this;
//
//    @Override
//    public IBinder onBind(Intent intent) {
//        Log.d(TAG, "onBind");
//        return null;
//    }
//
//    @Override
//    public void onRebind(Intent intent) {
//        Log.d(TAG, "onRebind");
//        super.onRebind(intent);
//    }
//
//    @Override
//    public boolean onUnbind(Intent intent) {
//        Log.d(TAG, "onUnbind");
//        return true;
//    }
//
//    @Override
//    public void onCreate() {
//        super.onCreate();
//        Log.d(TAG, "onCreate");
//
////        // Battery receiver
////        IntentFilter filter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
////        batteryReceiver = new BatteryReceiver();
////        registerReceiver(batteryReceiver, filter);
//    }
//
//    @Override
//    public int onStartCommand(Intent intent, int flags, int startId) {
//        Log.d(TAG, "onStartCommand");
//
////        // Working background thread
////        new Thread(new Runnable() {
////            public void run() {
////                while (Boolean.parseBoolean("true")) {
////                    try {
////                        Thread.sleep(2500);
////                    } catch (Exception e) {
////                        e.printStackTrace();
////                    }
////                }
////            }
////        }).start();
//
//        return Service.START_STICKY;
//    }
//
//    @Override
//    public void onTaskRemoved(Intent rootIntent) {
//        Log.d(TAG, "onTaskRemoved");
//        super.onTaskRemoved(rootIntent);
//    }
//
//    @Override
//    public void onLowMemory() {
//        Log.d(TAG, "onLowMemory");
//        super.onLowMemory();
//    }
//
//    @Override
//    public void onTrimMemory(int level) {
//        Log.d(TAG, "onTrimMemory " + level);
//        super.onTrimMemory(level);
//    }
//
//    @Override
//    public void onDestroy() {
//        Log.d(TAG, "onDestroy");
//        super.onDestroy();
//    }
//}
