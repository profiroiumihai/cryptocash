package com.crypto.cash.utils.network;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;

public class NetworkReceiver extends BroadcastReceiver {

    public static String TAG = NetworkReceiver.class.getCanonicalName();

    public int mInterval = 5000;
    public Handler mHandler;
    public Runnable mStatusChecker = null;
    public int nbFailedToConnect = 0;

    @Override
    public void onReceive(Context context, Intent intent) {

        Log.d(TAG, intent.getAction());

//        if (!Utils.isMyServiceRunning(context, MonitoringService.class)) {
//            context.startService(new Intent(context, MonitoringService.class));
//        }

        checkInternet(context);
    }

    public void startRepeatingTask() {
        mStatusChecker.run();
    }

    public void stopRepeatingTask() {
        mHandler.removeCallbacks(mStatusChecker);
    }

    public void checkInternet(final Context context) {
        mHandler = new Handler();

        mStatusChecker = new Runnable() {
            @Override
            public void run() {

                new Reachability(context, "google.com", 80, new ReachabilityInterface() {

                    @Override
                    public void onReachabilityTestPassed() {
                        NetworkUtil.HAS_INTERNET = true;
                        stopRepeatingTask();
                    }

                    @Override
                    public void onReachabilityTestFailed() {
                        NetworkUtil.HAS_INTERNET = false;

                        nbFailedToConnect++;

                        if (nbFailedToConnect > 10) {
                            stopRepeatingTask();
                        }
                    }
                }).execute();
                mHandler.postDelayed(mStatusChecker, mInterval);
            }
        };
        startRepeatingTask();
    }
}