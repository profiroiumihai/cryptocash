package com.crypto.cash.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Point;
import android.os.Build;
import android.os.PowerManager;
import android.support.v7.app.AppCompatActivity;
import android.view.Display;
import android.view.WindowManager;

import com.crypto.cash.ApplicationController;

/**
 * Created by paulsolea on 10/30/16.
 */
public class DeviceUtils {

    public static int width_d;
    public static int height_d;
    public static WindowManager wm = (WindowManager) ApplicationController.getInstance().getSystemService(Context.WINDOW_SERVICE);
    public static Display display = wm.getDefaultDisplay();
    public static boolean isScreenOn = false;

    @SuppressWarnings("deprecation")
    @SuppressLint("NewApi")
    public static int getScreenHeight() {
        Point size = new Point();
        display.getSize(size);
        height_d = size.y;
        return height_d;
    }

    @SuppressWarnings("deprecation")
    @SuppressLint("NewApi")
    public static int getScreenWidth() {
        Point size = new Point();
        display.getSize(size);
        width_d = size.x;
        return width_d;
    }

    @SuppressLint("NewApi")
    @SuppressWarnings("deprecation")
    public static boolean isScreenOn() {
        PowerManager pm = (PowerManager) ApplicationController.getInstance().getSystemService(Context.POWER_SERVICE);
        if (Build.VERSION.SDK_INT < 20 && pm != null) {
            isScreenOn = pm.isScreenOn();
        } else if (Build.VERSION.SDK_INT >= 20 && pm != null) {
            isScreenOn = pm.isInteractive();
        }
        return isScreenOn;
    }

    public static int getDisplayGoodHeightSize(Context mContext) {
        return getDisplaySize(mContext).x * 3 / 4;
    }

    public static Point getDisplaySize(Context mContext) {
        Point size = new Point(0, 0);
        if (mContext != null) {
            Display display = ((AppCompatActivity) mContext).getWindowManager().getDefaultDisplay();
            display.getSize(size);
        }
        return size;
    }
}
