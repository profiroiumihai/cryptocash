//package com.crypto.cash.utils;
//
//import android.app.Activity;
//import android.app.ActivityManager;
//import android.content.Context;
//import android.util.Log;
//
//import com.google.android.gms.common.ConnectionResult;
//import com.google.android.gms.common.GoogleApiAvailability;
//
//public class Utils {
//
//    public static String TAG = Utils.class.getCanonicalName();
//
//    public static boolean isMyServiceRunning(Context context, Class<?> serviceClass) {
//        boolean isRunning = false;
//
//        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
//        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
//            if (serviceClass.getName().equals(service.service.getClassName())) {
//                isRunning = true;
//            }
//        }
//        Log.d(TAG, "Service " + serviceClass.getCanonicalName() + " running: " +  isRunning);
//
//        return isRunning;
//    }
//
//    public static boolean isGooglePlayServicesAvailable(Activity activity) {
//        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
//        int status = googleApiAvailability.isGooglePlayServicesAvailable(activity);
//        if(status != ConnectionResult.SUCCESS) {
//            if(googleApiAvailability.isUserResolvableError(status)) {
//                googleApiAvailability.getErrorDialog(activity, status, 2404).show();
//            }
//            return false;
//        }
//        return true;
//    }
//}
