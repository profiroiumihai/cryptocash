package com.crypto.cash;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.util.Log;

//import android.support.multidex.MultiDex;

public class ApplicationController extends Application {

    public static String TAG = ApplicationController.class.getCanonicalName();

    private static ApplicationController mInstance;

    public static synchronized ApplicationController getInstance() {
        return mInstance;
    }

    public static void sendInternalIntent(String action, String message) {
        Intent intent = new Intent(action);
        intent.setPackage(getInstance().getPackageName());
        intent.putExtra("message", message);
        getInstance().sendBroadcast(intent);
    }

    public static String getApplicationLabel(Context context) throws Exception {
        PackageManager pm = context.getPackageManager();
        ApplicationInfo applicationInfo = pm.getApplicationInfo(context.getApplicationInfo().packageName, 0);
        return (String) (applicationInfo != null ? pm.getApplicationLabel(applicationInfo) : "Unknown");
    }

    public static int getApplicationVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    public static Bitmap getApplicationIcon(Context context, String packageName) throws Exception {
        return ((BitmapDrawable) context.getPackageManager().getApplicationIcon(packageName)).getBitmap();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
//        MultiDex.install(this);

        Log.d(TAG, "attachBaseContext");
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Log.d(TAG, "onCreate");

        mInstance = this;

    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        Log.d(TAG, "onLowMemory");
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        Log.d(TAG, "onTrimMemory");
    }
}
