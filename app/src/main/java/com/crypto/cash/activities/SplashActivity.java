package com.crypto.cash.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPropertyAnimatorCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.crypto.cash.R;
import com.schibsted.spain.parallaxlayerlayout.ParallaxLayerLayout;
import com.schibsted.spain.parallaxlayerlayout.SensorTranslationUpdater;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.graphics.Typeface.BOLD;

public class SplashActivity extends AppCompatActivity {

    public static final int STARTUP_DELAY = 1000;
    public static final int ANIM_ITEM_DURATION = 3000;
    public static final int ITEM_DELAY = 1000;
    Activity activity = this;
    @BindView(R.id.splash_iv_logo)
    ImageView logoImageView;
    @BindView(R.id.splash_container)
    ViewGroup splashContainer;
    @BindView(R.id.first_text_view)
    TextView firstTextView;
    @BindView(R.id.second_text_view)
    TextSwitcher secondTextView;
    @BindView(R.id.splash_button_intro)
    Button buttonIntro;
    @BindView(R.id.splash_button_login)
    Button buttonLogin;

    SensorTranslationUpdater sensorTranslationUpdater;


    private boolean animationStarted = false;

    private int counter = 0;
    private String[] words = new String[]{"awesome", "private", "instant"};
    private Handler mHandler;
    private Runnable mUpdateUI;
    private ViewSwitcher.ViewFactory mFactory = new ViewSwitcher.ViewFactory() {
        @Override
        public View makeView() {
            TextView t = new TextView(activity);
            t.setTextAppearance(activity, android.R.style.TextAppearance_Large_Inverse);
            t.setTextColor(getResources().getColor(R.color.colorAccent));
            t.setTypeface(null, BOLD);
            return t;
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        setTheme(R.style.AppTheme);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);

        ButterKnife.bind(activity);

        secondTextView.setFactory(mFactory);
        Animation in = AnimationUtils.loadAnimation(activity, android.R.anim.fade_in);
        Animation out = AnimationUtils.loadAnimation(activity, android.R.anim.fade_out);
        secondTextView.setInAnimation(in);
        secondTextView.setOutAnimation(out);
        mHandler = new Handler();
        mUpdateUI = new Runnable() {
            @Override
            public void run() {
                updateCounter();
                mHandler.postDelayed(mUpdateUI, 1500);
            }
        };
        mHandler.post(mUpdateUI);

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(activity, AuthActivity.class));
            }
        });

        buttonIntro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(activity, IntroActivity.class));
            }
        });

        ParallaxLayerLayout parallaxLayout= findViewById(R.id.parallax);
        sensorTranslationUpdater= new SensorTranslationUpdater(this);
        parallaxLayout.setTranslationUpdater(sensorTranslationUpdater);

    }

    @Override
    protected void onResume() {
        super.onResume();
        sensorTranslationUpdater.registerSensorManager();
    }


    private void updateCounter() {
        counter++;
        int index = counter % words.length;
        if (secondTextView != null) {
            secondTextView.setText(String.valueOf(words[index]));
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        if (!hasFocus || animationStarted) {
            return;
        }
        introAnimation();
        super.onWindowFocusChanged(hasFocus);
    }

    private void introAnimation() {
        ViewCompat.animate(logoImageView)
                .translationY(-250)
                .setStartDelay(STARTUP_DELAY)
                .setDuration(ANIM_ITEM_DURATION).setInterpolator(
                new DecelerateInterpolator(1.2f)).start();

        for (int i = 0; i < splashContainer.getChildCount(); i++) {
            View v = splashContainer.getChildAt(i);
            ViewPropertyAnimatorCompat viewAnimator;

            if (!(v instanceof Button)) {
                viewAnimator = ViewCompat.animate(v)
                        .translationY(50).alpha(1)
                        .setStartDelay((ITEM_DELAY * i) + 500)
                        .setDuration(3000);
            } else {
                viewAnimator = ViewCompat.animate(v)
                        .scaleY(1).scaleX(1)
                        .setStartDelay((ITEM_DELAY * i) + 500)
                        .setDuration(2000);
            }

            viewAnimator.setInterpolator(new DecelerateInterpolator()).start();
        }
    }

//    if (MultiSharedPrefs.getAppToken().equals("")) {
//        intent = new Intent(mContext, AuthActivity.class);
//    } else {
//        intent = new Intent(mContext, MainActivity.class);
//    }
}
