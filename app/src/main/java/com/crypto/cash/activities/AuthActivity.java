package com.crypto.cash.activities;

import android.app.Activity;
import android.os.Bundle;

import com.crypto.cash.R;
import com.schibsted.spain.parallaxlayerlayout.ParallaxLayerLayout;
import com.schibsted.spain.parallaxlayerlayout.SensorTranslationUpdater;

/**
 * Created by mike on 2/11/2018.
 */

public class AuthActivity extends Activity {


    SensorTranslationUpdater sensorTranslationUpdater;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        ParallaxLayerLayout parallaxLayout= findViewById(R.id.parallax);

        sensorTranslationUpdater = new SensorTranslationUpdater(this);
        parallaxLayout.setTranslationUpdater(sensorTranslationUpdater);


    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorTranslationUpdater.unregisterSensorManager();
    }



    @Override
    protected void onResume() {
        super.onResume();
        sensorTranslationUpdater.registerSensorManager();
    }


}
