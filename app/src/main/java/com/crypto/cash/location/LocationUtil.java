//package com.crypto.cash.location;
//
//import android.app.Activity;
//import android.content.Context;
//import android.content.IntentSender;
//import android.location.Criteria;
//import android.location.Location;
//import android.location.LocationManager;
//import android.os.Bundle;
//import android.support.annotation.NonNull;
//import android.support.annotation.Nullable;
//import android.support.design.widget.FloatingActionButton;
//import android.util.Log;
//import android.widget.Toast;
//
//import com.google.android.gms.common.ConnectionResult;
//import com.google.android.gms.common.api.GoogleApiClient;
//import com.google.android.gms.common.api.PendingResult;
//import com.google.android.gms.common.api.ResultCallback;
//import com.google.android.gms.common.api.Status;
//import com.google.android.gms.location.LocationRequest;
//import com.google.android.gms.location.LocationServices;
//import com.google.android.gms.location.LocationSettingsRequest;
//import com.google.android.gms.location.LocationSettingsResult;
//import com.google.android.gms.location.LocationSettingsStates;
//import com.google.android.gms.location.LocationSettingsStatusCodes;
//
//import ro.android.ecotrans.R;
//import ro.android.ecotrans.activities.NavigationActivity;
//
///**
// * Created by paulsolea on 11/1/16.
// */
//public class LocationUtil {
//
//    public static String TAG = LocationUtil.class.getCanonicalName();
//
//    private static final int TIME_REQUEST = 30 * 1000; // 30 seconds
//
//    public static String getCustomProvider(LocationManager locationManager,
//                                           int powerConsumption,
//                                           int accuracy,
//                                           boolean speedRequired,
//                                           boolean altitudeRequired,
//                                           boolean bearingRequired,
//                                           boolean costAllowed) {
//
//        Criteria criteria = new Criteria();
//
//        // Chose your desired power consumption level.
//        switch (powerConsumption) {
//            case 2:
//                criteria.setPowerRequirement(Criteria.POWER_HIGH);
//                break;
//            case 1:
//                criteria.setPowerRequirement(Criteria.POWER_MEDIUM);
//                break;
//            case 0:
//                criteria.setPowerRequirement(Criteria.POWER_LOW);
//                break;
//            default:
//                criteria.setPowerRequirement(Criteria.POWER_LOW);
//                break;
//        }
//
//        // Choose your accuracy requirement.
//        switch (accuracy) {
//            case 0:
//                criteria.setAccuracy(Criteria.ACCURACY_COARSE);
//                break;
//            case 1:
//                criteria.setAccuracy(Criteria.ACCURACY_FINE);
//                break;
//            default:
//                criteria.setAccuracy(Criteria.ACCURACY_COARSE);
//                break;
//        }
//
//        criteria.setSpeedRequired(speedRequired); // Chose if speed for first location fix is required.
//
//        criteria.setAltitudeRequired(altitudeRequired); // Choose if you use altitude.
//
//        criteria.setBearingRequired(bearingRequired); // Choose if you use bearing.
//
//        criteria.setCostAllowed(costAllowed); // Choose if this provider can waste money :-)
//
//        // Provide your criteria and flag enabledOnly that tells
//        // LocationManager only to return active providers.
//        return locationManager.getBestProvider(criteria, true);
//    }
//
//    public static float getAverageSpeed(float distance, float timeTaken) {
//        float speed = 0;
//        if (distance > 0) {
//            float distancePerSecond = timeTaken > 0 ? distance / timeTaken : 0;
//            float distancePerMinute = distancePerSecond * 60;
//            float distancePerHour = distancePerMinute * 60;
//            speed = distancePerHour > 0 ? (distancePerHour / 1000) : 0;
//        }
//        return speed;
//    }
//
//    public static boolean isBetterLocation(Location location, Location currentBestLocation) {
//        if (currentBestLocation == null) {
//            return true;
//        }
//
//        // Check whether the new location fix is newer or older
//        long timeDelta = location.getTime() - currentBestLocation.getTime();
//        boolean isSignificantlyNewer = timeDelta > TIME_REQUEST;
//        boolean isSignificantlyOlder = timeDelta < - TIME_REQUEST;
//        boolean isNewer = timeDelta > 0;
//
//        // If it's been more than two minutes since the current location, use the new location
//        // because the user has likely moved
//        if (isSignificantlyNewer) {
//            return true;
//            // If the new location is more than two minutes older, it must be worse
//        } else if (isSignificantlyOlder) {
//            return false;
//        }
//
//        // Check whether the new location fix is more or less accurate
//        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
//        boolean isLessAccurate = accuracyDelta > 0;
//        boolean isMoreAccurate = accuracyDelta < 0;
//        boolean isSignificantlyLessAccurate = accuracyDelta > 200;
//
//        // Check if the old and new location are from the same provider
//        boolean isFromSameProvider = isSameProvider(location.getProvider(), currentBestLocation.getProvider());
//
//        // Determine location quality using a combination of timeliness and accuracy
//        if (isMoreAccurate) {
//            return true;
//        } else if (isNewer && !isLessAccurate) {
//            return true;
//        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
//            return true;
//        }
//        return false;
//    }
//
//    public static boolean isSameProvider(String provider1, String provider2) {
//        if (provider1 == null) {
//            return provider2 == null;
//        }
//        return provider1.equals(provider2);
//    }
//
//    public static boolean isGPSEnabled(Context context) {
//        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
//
//        Log.d(TAG, locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) + "");
//
//        return (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER));
//    }
//
//    public static void requireGPSDialog(final Activity activity) {
//        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(activity)
//                .addApi(LocationServices.API)
//                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
//                    @Override
//                    public void onConnected(@Nullable Bundle bundle) {
//                    }
//
//                    @Override
//                    public void onConnectionSuspended(int i) {
//                    }
//                })
//                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
//                    @Override
//                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
//                    }
//                }).build();
//        googleApiClient.connect();
//
//        LocationRequest locationRequest = LocationRequest.create();
//        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
//        locationRequest.setInterval(30 * 1000);
//        locationRequest.setFastestInterval(5 * 1000);
//        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
//                .addLocationRequest(locationRequest);
//        builder.setAlwaysShow(true); //this is the key ingredient
//
//        PendingResult<LocationSettingsResult> result =
//                LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
//        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
//            @Override
//            public void onResult(LocationSettingsResult result) {
//                final Status status = result.getStatus();
//                final LocationSettingsStates state = result.getLocationSettingsStates();
//                switch (status.getStatusCode()) {
//                    case LocationSettingsStatusCodes.SUCCESS:
//                        Log.d(TAG, "SUCCESS");
//                        // All location settings are satisfied. The client can initialize location
//                        // requests here.
//                        break;
//                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
//                        Log.d(TAG, "RESOLUTION_REQUIRED");
//                        // Location settings are not satisfied. But could be fixed by showing the user
//                        // a dialog.
//                        try {
//                            // Show the dialog by calling startResolutionForResult(),
//                            // and check the result in onActivityResult().
//                            status.startResolutionForResult(activity, NavigationActivity.REQUEST_CHECK_SETTINGS);
//                        } catch (IntentSender.SendIntentException e) {
//                            Toast.makeText(activity, activity.getResources().getString(R.string.status_gps_error), Toast.LENGTH_LONG).show();
//                            e.printStackTrace();
//                            // Ignore the error.
//                        }
//                        break;
//                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
//                        Toast.makeText(activity, activity.getResources().getString(R.string.status_gps_error), Toast.LENGTH_LONG).show();
//                        Log.d(TAG, "SETTINGS_CHANGE_UNAVAILABLE");
//                        // Location settings are not satisfied. However, we have no way to fix the
//                        // settings so we won't show the dialog.
//                        break;
//                }
//            }
//        });
//    }
//}
