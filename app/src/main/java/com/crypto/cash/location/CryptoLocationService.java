//package com.crypto.cash.location;
//
//import android.app.PendingIntent;
//import android.app.Service;
//import android.content.BroadcastReceiver;
//import android.content.Context;
//import android.content.Intent;
//import android.graphics.Color;
//import android.location.LocationManager;
//import android.media.RingtoneManager;
//import android.os.IBinder;
//import android.support.annotation.Nullable;
//
//import android.support.v4.app.NotificationCompat;
//import android.util.Log;
//
//import com.google.android.gms.common.api.GoogleApiClient;
//import com.google.android.gms.location.LocationServices;
//
//import ro.android.ecotrans.ApplicationController;
//import ro.android.ecotrans.R;
//import ro.android.ecotrans.activities.NavigationActivity;
//import ro.android.ecotrans.prefs.MultiSharedPrefs;
//
//public class CryptoLocationService extends Service {
//
//    public static final String TAG = CryptoLocationService.class.getCanonicalName();
//
//    public static final int NOTIFICATION_ID = 123;
//
//    public Context context = this;
//
//    public LocationManager locationManager;
//    public CryptoLocationListener cryptoLocationListener;
//    public CryptoGMSLocationListener cryptoGMSLocationListener;
//    public GoogleApiClient googleLocationApiClient;
//
//    public static final int REFRESH_TIME_LOCATION = 10 * 1000; // 10 seconds
//
//    // Battery receiver
//    public BroadcastReceiver batteryReceiver;
//
//    public NotificationCompat.Builder notificationBuilder;
//    public NotificationCompat.BigPictureStyle bigPictureStyle;
//
//    @Nullable
//    @Override
//    public IBinder onBind(Intent intent) {
//        return null;
//    }
//
//    @Override
//    public void onCreate() {
//        super.onCreate();
//        Log.d(TAG, "onCreate");
//
//        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
//
////        // Battery receiver
////        IntentFilter filter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
////        batteryReceiver = new BatteryReceiver();
////        registerReceiver(batteryReceiver, filter);
//
//        // Decide what provider to use for location
//        if (MultiSharedPrefs.getPrefs("gms_info").getBoolean("gms_status", true)) {
//            // GMS Location Listener
//            // For automatic switching, we need to know battery usage
//            cryptoGMSLocationListener = new CryptoGMSLocationListener(context, 3);
//        } else {
//            // Android Location Listener
//            // For automatic switching, we need to know battery usage
//            cryptoLocationListener = new CryptoLocationListener(context, locationManager);
//        }
//
//        // Starting in foreground
//        startForegroundService(this, "EcoTrans", "Conduceti cu grija!");
//    }
//
//    @Override
//    public int onStartCommand(Intent intent, int flags, int startId) {
//        Log.d(TAG, "onStartCommand");
//        Log.d(TAG, "onStartCommand Intent: " + intent.getAction());
//
//        // First try with GMS location listener
//        if (cryptoGMSLocationListener != null) {
//            googleLocationApiClient = cryptoGMSLocationListener.getGoogleApiClient();
//            googleLocationApiClient.connect();
//
//        // If not initialized, try with Android location listener
//        } else if (cryptoLocationListener != null) {
//
//            // Choose best Android location provider
//            if ((locationManager.getAllProviders().contains(LocationManager.GPS_PROVIDER)
//                    && locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))) {
//                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, REFRESH_TIME_LOCATION, 0, cryptoLocationListener);
//            }
//            // Fallback on Network & Passive
//
//            // Choose custom Android location provider
//            locationManager.requestLocationUpdates(
//                    LocationUtil.getCustomProvider(
//                            locationManager,
//                            2,
//                            1,
//                            true, false, false, false),
//                    REFRESH_TIME_LOCATION,
//                    0,
//                    cryptoLocationListener);
//        }
//
//        return START_NOT_STICKY;
//    }
//
//    @Override
//    public void onTaskRemoved(Intent rootIntent) {
//        Log.d(TAG, "onTaskRemoved");
//    }
//
//    @Override
//    public void onLowMemory() {
//        Log.d(TAG, "onLowMemory");
//    }
//
//    @Override
//    public void onTrimMemory(int level) {
//        Log.d(TAG, "onTrimMemory " + level);
//    }
//
//    @Override
//    public void onDestroy() {
//        Log.d(TAG, "onDestroy");
//        if (cryptoGMSLocationListener != null && googleLocationApiClient != null && googleLocationApiClient.isConnected() ) {
//            LocationServices.FusedLocationApi.removeLocationUpdates(googleLocationApiClient, cryptoGMSLocationListener);
//            googleLocationApiClient.disconnect();
//        }
//        if (locationManager != null) {
//            locationManager.removeUpdates(cryptoLocationListener);
//        }
//        if (batteryReceiver != null) {
//            unregisterReceiver(batteryReceiver);
//        }
//        super.onDestroy();
//    }
//
//    public void startForegroundService(final Service service,
//                                       final String contentTitle,
//                                       final String contentText) {
//
//        PendingIntent contentIntent = PendingIntent.getActivity(
//                ApplicationController.getInstance(),
//                0,
//                new Intent(context, NavigationActivity.class),
//                PendingIntent.FLAG_UPDATE_CURRENT);
//
//        notificationBuilder = new NotificationCompat.Builder(context)
//                .setContentTitle(contentTitle)
//                .setContentText(contentText)
//
//                .setSmallIcon(R.mipmap.ic_launcher)
////                .setLargeIcon(BitmapFactory.decodeResource(getResources(), android.R.drawable.arrow_up_float))
//
//                .setWhen(System.currentTimeMillis())
//
//                .setContentIntent(contentIntent)
//
//                .setVibrate(new long[] {500})
//                .setLights(Color.GREEN, 3000, 1000)
//                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
//
////        bigPictureStyle = new NotificationCompat.BigPictureStyle();
////        bigPictureStyle.bigPicture(b);
////        notificationBuilder.setStyle(bigPictureStyle);
//        service.startForeground(NOTIFICATION_ID, notificationBuilder.build());
//
////        Volley.newRequestQueue(context).add(
////                new ImageRequest("http://icons.iconarchive.com/icons/paomedia/small-n-flat/1024/sign-check-icon.png",
////                        new Response.Listener<Bitmap>() {
////
////                            @Override
////                            public void onResponse(Bitmap b) {
////                                Log.d(TAG, "Volley onResponse");
////
////
////                                bigPictureStyle = new NotificationCompat.BigPictureStyle();
////                                bigPictureStyle.bigPicture(b);
////
////                                notificationBuilder.setStyle(bigPictureStyle);
////
////                                service.startForeground(1, notificationBuilder.build());
////                            }
////                        },
////                        DeviceUtils.getScreenWidth(),
////                        200,
////                        ImageView.ScaleType.CENTER,
////                        null,
////                        new Response.ErrorListener() {
////                            @Override
////                            public void onErrorResponse(VolleyError volleyError) {
////                                Log.d(TAG, "Volley " + volleyError);
////                                volleyError.printStackTrace();
////                            }
////                        }));
//    }
//
////    public void updateNotification(Context context, String contentTitle, String contentText) {
////        final NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
////
////        PendingIntent contentIntent = PendingIntent.getActivity(
////                ApplicationController.getInstance(),
////                0,
////                new Intent(context, NavigationActivity.class),
////                PendingIntent.FLAG_UPDATE_CURRENT);
////
////        notificationBuilder = new NotificationCompat.Builder(context)
////                .setContentTitle(contentTitle)
////                .setContentText(contentText)
////
////                .setSmallIcon(R.mipmap.ic_launcher)
////                .setLargeIcon(BitmapFactory.decodeResource(getResources(), android.R.drawable.arrow_up_float))
////
////                .setWhen(0)
////
////                .setContentIntent(contentIntent)
////
////                //.setVibrate(new long[] {500})
////                .setLights(Color.GREEN, 3000, 1000)
////                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
////
////        Volley.newRequestQueue(context).add(
////                new ImageRequest("http://icons.iconarchive.com/icons/paomedia/small-n-flat/1024/sign-check-icon.png",
////                        new Response.Listener<Bitmap>() {
////
////                            @Override
////                            public void onResponse(Bitmap b) {
////                                Log.d(TAG, "Volley onResponse");
////
////
////                                bigPictureStyle = new NotificationCompat.BigPictureStyle();
////                                bigPictureStyle.bigPicture(b);
////
////                                notificationBuilder.setStyle(bigPictureStyle);
////
////                                mNotificationManager.notify(1, notificationBuilder.build());
////                            }
////                        },
////                        DeviceUtils.getScreenWidth(),
////                        200,
////                        ImageView.ScaleType.CENTER,
////                        null,
////                        new Response.ErrorListener() {
////                            @Override
////                            public void onErrorResponse(VolleyError volleyError) {
////                                Log.d(TAG, "Volley " + volleyError);
////                                volleyError.printStackTrace();
////                            }
////                        }));
////    }
//}
