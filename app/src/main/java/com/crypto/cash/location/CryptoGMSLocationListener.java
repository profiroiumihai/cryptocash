//package com.crypto.cash.location;
//
//import android.app.Activity;
//import android.content.Context;
//import android.content.IntentSender;
//import android.location.Location;
//import android.os.Bundle;
//import android.support.annotation.NonNull;
//import android.support.annotation.Nullable;
//import android.util.Log;
//import android.widget.Toast;
//
//import com.google.android.gms.common.ConnectionResult;
//import com.google.android.gms.common.GooglePlayServicesUtil;
//import com.google.android.gms.common.api.GoogleApiClient;
//import com.google.android.gms.location.ActivityRecognition;
//import com.google.android.gms.location.LocationListener;
//import com.google.android.gms.location.LocationRequest;
//import com.google.android.gms.location.LocationServices;
//
//import java.text.DecimalFormat;
//
//import ro.android.ecotrans.ApplicationController;
//import ro.android.ecotrans.fcm.api.RestClient;
//import ro.android.ecotrans.prefs.MultiSharedPrefs;
//import ro.android.ecotrans.structure.models.SuccessResponseModel;
//import rx.Observable;
//import rx.Subscriber;
//import rx.android.schedulers.AndroidSchedulers;
//import rx.schedulers.Schedulers;
//
//public class CryptoGMSLocationListener implements
//        GoogleApiClient.ConnectionCallbacks,
//        GoogleApiClient.OnConnectionFailedListener,
//        LocationListener {
//
//    public static String TAG = CryptoGMSLocationListener.class.getCanonicalName();
//
//    public Context context;
//    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
//    private GoogleApiClient mGoogleApiClient;
//    private LocationRequest mLocationRequest;
//
//    public Location previousBestLocation;
//
//    public CryptoGMSLocationListener(Context context, int priority) {
//
//        this.context = context;
//
//        mGoogleApiClient = new GoogleApiClient.Builder(context)
//                .addApi(LocationServices.API)
//                .addApiIfAvailable(ActivityRecognition.API)
//                .addConnectionCallbacks(this)
//                .addOnConnectionFailedListener(this)
//                .build();
//
//        mLocationRequest = LocationRequest.create();
//
//        // Create the LocationRequest object
//        switch (priority) {
//            case 0:
//                mLocationRequest.setPriority(LocationRequest.PRIORITY_NO_POWER);
//                break;
//            case 1:
//                mLocationRequest.setPriority(LocationRequest.PRIORITY_LOW_POWER);
//                break;
//            case 2:
//                mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
//                break;
//            case 3:
//                mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
//                break;
//            default:
//                mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
//                break;
//
//        }
//
//        mLocationRequest.setInterval(10 * 1000)        // 10 seconds, in milliseconds
//                        .setFastestInterval(2 * 1000); // 1 second, in milliseconds
//    }
//
//    public GoogleApiClient getGoogleApiClient() {
//        return this.mGoogleApiClient;
//    }
//
//    @Override
//    public void onConnected(@Nullable Bundle bundle) {
//        Log.d(TAG, "onConnected");
//
//        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
//
//        if (location == null) {
//            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
//        } else {
//            Toast.makeText(context, "GMS Latitude: " + location.getLatitude() + " and longitude: " + location.getLongitude() + "", Toast.LENGTH_LONG).show();
//        }
//
//    }
//
//    @Override
//    public void onConnectionSuspended(int i) {
//        Log.d(TAG, "onConnectionSuspended");
//
//    }
//
//    @Override
//    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
//        Log.d(TAG, "onConnectionFailed");
//
//        /*
//        * * Google Play services can resolve some errors it detects.
//             * If the error has a resolution, try sending an Intent to
//             * start a Google Play services activity that can resolve
//             * error.
//             */
//        if (connectionResult.hasResolution()) {
//            try {
//                // Start an Activity that tries to resolve the error
//                connectionResult.startResolutionForResult((Activity) context, CONNECTION_FAILURE_RESOLUTION_REQUEST);
//                    /*
//                     * Thrown if Google Play services canceled the original
//                     * PendingIntent
//                     */
//            } catch (IntentSender.SendIntentException e) {
//                // Log the error
//                Log.d(TAG, e.getMessage());
//                Toast.makeText(context,
//                        "Location services connection failed with code " + connectionResult.getErrorCode(),
//                        Toast.LENGTH_LONG).show();
//                e.printStackTrace();
//            }
//        } else {
//                /*
//                 * If no resolution is available, display a dialog to the
//                 * user with the error.
//                 */
//            GooglePlayServicesUtil.getErrorDialog(999999999, (Activity) context, GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_VERSION_CODE).show();
//            Log.d(TAG, "Location services connection failed with code " + connectionResult.getErrorCode());
//        }
//
//    }
//
//    @Override
//    public void onLocationChanged(Location loc) {
//        float speed = 0;
//        if (LocationUtil.isBetterLocation(loc, previousBestLocation)) {
//            if (previousBestLocation != null) {
//                float distance = loc.distanceTo(previousBestLocation);
//                float timeTaken = ((loc.getTime() - previousBestLocation.getTime()) / 1000);
//                if (timeTaken > 0) {
//                    speed = LocationUtil.getAverageSpeed(distance, timeTaken);
//                }
//
//                if (speed >= 10) {
//                    DecimalFormat df = new DecimalFormat("#.##");
//                    Toast.makeText(ApplicationController.getInstance(), "Average speed : " + df.format(speed) + " km/h", Toast.LENGTH_SHORT).show();
//                } else {
//                    Toast.makeText(ApplicationController.getInstance(), "Average speed : 0", Toast.LENGTH_SHORT).show();
//                }
//            }
//            previousBestLocation = loc;
//
//            MultiSharedPrefs.setLatitude(String.valueOf(previousBestLocation.getLatitude()));
//            MultiSharedPrefs.setLongitude(String.valueOf(previousBestLocation.getLongitude()));
//
//            Observable<SuccessResponseModel> setPositionObservable = RestClient.getInstance().getApiService()
//                    .setPosition(
//                            MultiSharedPrefs.getAppToken(),
//                            loc.getLatitude(),
//                            loc.getLongitude(),
//                            speed > 0 ? speed : 0);
//
//            setPositionObservable
//                    .observeOn(AndroidSchedulers.mainThread())
//                    .subscribeOn(Schedulers.newThread())
//                    .subscribe(new Subscriber<SuccessResponseModel>() {
//                        @Override
//                        public void onCompleted() {
//                        }
//
//                        @Override
//                        public void onError(Throwable e) {
//                        }
//
//                        @Override
//                        public void onNext(SuccessResponseModel successResponseModel) {
//                            Toast.makeText(ApplicationController.getInstance(), successResponseModel.getSuccess(), Toast.LENGTH_SHORT).show();
//                        }
//                    });
//        }
//    }
//}