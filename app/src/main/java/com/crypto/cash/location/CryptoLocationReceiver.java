//package com.crypto.cash.location;
//
//import android.content.BroadcastReceiver;
//import android.content.Context;
//import android.content.Intent;
//import android.location.LocationManager;
//import android.util.Log;
//
//import ro.android.ecotrans.utils.CommonUtilities;
//import ro.android.ecotrans.utils.Utils;
//
//public class CryptoLocationReceiver extends BroadcastReceiver {
//
//    public static String TAG = CryptoLocationReceiver.class.getCanonicalName();
//
//    @Override
//    public void onReceive(Context context, Intent intent) {
//        Log.d(TAG, intent.getAction());
//
//        if (!Utils.isMyServiceRunning(context, CryptoLocationService.class) && intent.getAction().equals(LocationManager.PROVIDERS_CHANGED_ACTION)) {
//            context.startService(new Intent(context, CryptoLocationService.class));
//        }
//    }
//}