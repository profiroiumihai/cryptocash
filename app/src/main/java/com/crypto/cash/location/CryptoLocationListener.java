//package com.crypto.cash.location;
//
//import android.content.Context;
//import android.location.GpsSatellite;
//import android.location.GpsStatus;
//import android.location.Location;
//import android.location.LocationManager;
//import android.os.Bundle;
//import android.util.Log;
//import android.widget.Toast;
//
//import java.text.DecimalFormat;
//import java.util.Iterator;
//
//import ro.android.ecotrans.ApplicationController;
//import ro.android.ecotrans.fcm.api.RestClient;
//import ro.android.ecotrans.prefs.MultiSharedPrefs;
//import ro.android.ecotrans.structure.models.SuccessResponseModel;
//import rx.Observable;
//import rx.Subscriber;
//import rx.android.schedulers.AndroidSchedulers;
//import rx.schedulers.Schedulers;
//
//public class CryptoLocationListener implements
//        android.location.LocationListener,
//        GpsStatus.Listener,
//        GpsStatus.NmeaListener {
//
//    public static String TAG = CryptoLocationListener.class.getCanonicalName();
//    public Location previousBestLocation;
//    public LocationManager locationManager;
//
//    public Context context;
//
//    public CryptoLocationListener(Context context, LocationManager locationManager) {
//        this.context = context;
//        this.locationManager = locationManager;
//    }
//
//    @Override
//    public void onProviderDisabled(String provider) {
//        Log.d(TAG, "onProviderDisabled " + provider);
//        // Send 2 server
//    }
//
//    @Override
//    public void onProviderEnabled(String provider) {
//        Log.d(TAG, "onProviderEnabled " + provider);
//        // Send 2 server
//    }
//
//    @Override
//    public void onStatusChanged(String provider, int status, Bundle extras) {
//        Log.d(TAG, "onStatusChanged " + provider + " " + status);
//        // Send 2 server
//    }
//
//    @Override
//    public void onGpsStatusChanged(int event) {
//        GpsStatus gpsStatus = locationManager.getGpsStatus(null);
//
//        if(gpsStatus != null) {
//            Iterable<GpsSatellite>satellites = gpsStatus.getSatellites();
//            Iterator<GpsSatellite> sat = satellites.iterator();
//            String lSatellites = null;
//            int i = 0;
//            while (sat.hasNext()) {
//                GpsSatellite satellite = sat.next();
//                lSatellites = "Satellite" + (i++) + ": "
//                        + satellite.getPrn() + ","
//                        + satellite.usedInFix() + ","
//                        + satellite.getSnr() + ","
//                        + satellite.getAzimuth() + ","
//                        + satellite.getElevation()+ "\n\n";
//
//                Log.d(TAG, lSatellites);
//            }
//        }
//    }
//
//    @Override
//    public void onNmeaReceived(long timestamp, String nmea) {
//        Log.d(TAG, timestamp + "");
//    }
//
//    @Override
//    public void onLocationChanged(final Location loc) {
//        float speed = 0;
//        if (LocationUtil.isBetterLocation(loc, previousBestLocation)) {
//            if (previousBestLocation != null) {
//                float distance = loc.distanceTo(previousBestLocation);
//                float timeTaken = ((loc.getTime() - previousBestLocation.getTime()) / 1000);
//                if (timeTaken > 0) {
//                    speed = LocationUtil.getAverageSpeed(distance, timeTaken);
//                }
//
//                if (speed >= 10) {
//                    DecimalFormat df = new DecimalFormat("#.##");
//                    Toast.makeText(ApplicationController.getInstance(), "Average speed : " + df.format(speed) + " km/h", Toast.LENGTH_SHORT).show();
//                } else {
//                    Toast.makeText(ApplicationController.getInstance(), "Average speed : 0", Toast.LENGTH_SHORT).show();
//                }
//            }
//            previousBestLocation = loc;
//
//            MultiSharedPrefs.setLatitude(String.valueOf(previousBestLocation.getLatitude()));
//            MultiSharedPrefs.setLongitude(String.valueOf(previousBestLocation.getLongitude()));
//
//            Observable<SuccessResponseModel> setPositionObservable = RestClient.getInstance().getApiService()
//                    .setPosition(
//                            MultiSharedPrefs.getAppToken(),
//                            loc.getLatitude(),
//                            loc.getLongitude(),
//                            speed > 0 ? speed : 0);
//
//            setPositionObservable
//                    .observeOn(AndroidSchedulers.mainThread())
//                    .subscribeOn(Schedulers.newThread())
//                    .subscribe(new Subscriber<SuccessResponseModel>() {
//                        @Override
//                        public void onCompleted() {
//                        }
//
//                        @Override
//                        public void onError(Throwable e) {
//                        }
//
//                        @Override
//                        public void onNext(SuccessResponseModel successResponseModel) {
//                            Toast.makeText(ApplicationController.getInstance(), successResponseModel.getSuccess(), Toast.LENGTH_SHORT).show();
//                        }
//                    });
//        }
//    }
//}