package com.crypto.cash.prefs;

import android.content.Context;

import net.grandcentrix.tray.AppPreferences;
import net.grandcentrix.tray.core.ItemNotFoundException;

public class TrayPrefs extends AppPreferences {

    public String tag = "Default";

    public TrayPrefs(Context context) {
        super(context);
    }

    public TrayPrefs(Context context, String tag) {
        super(context);
        this.tag = tag;
    }

    public void putString(String key, String value) {
        super.put(getTag(key), value);
    }

    public void putBoolean(String key, boolean value) {
        super.put(getTag(key), value);
    }

    public void putFloat(String key, float value) {
        super.put(getTag(key), value);
    }

    public void putInt(String key, int value) {
        super.put(getTag(key), value);
    }

    public void putLong(String key, long value) {
        super.put(getTag(key), value);
    }

    public String getString(String key, String defaultValue) {
        try {
            return super.getString(getTag(key));
        } catch (ItemNotFoundException e) {
            e.printStackTrace();
            return defaultValue;
        }
    }

    public boolean getBoolean(String key, boolean defaultValue) {
        try {
            return super.getBoolean(getTag(key));
        } catch (ItemNotFoundException e) {
            e.printStackTrace();
            return defaultValue;
        }
    }

    public float getFloat(String key, float defaultValue) {
        try {
            return super.getFloat(getTag(key));
        } catch (ItemNotFoundException e) {
            e.printStackTrace();
            return defaultValue;
        }
    }

    public int getInt(String key, int defaultValue) {
        try {
            return super.getInt(getTag(key));
        } catch (ItemNotFoundException e) {
            e.printStackTrace();
            return defaultValue;
        }
    }

    public long getLong(String key, long defaultValue) {
        try {
            return super.getLong(getTag(key));
        } catch (ItemNotFoundException e) {
            e.printStackTrace();
            return defaultValue;
        }
    }

    public boolean remove(String key) {
        return super.remove(key);
    }

    public String getTag(String key) {
        return this.tag + "_" + key;
    }

}
