package com.crypto.cash.prefs;

import com.crypto.cash.ApplicationController;

import java.util.HashMap;

public class MultiSharedPrefs {

//    public static final String INTENT_EXTRA_LAST_UPDATE_DB_TIME_CATEGORY = "INTENT_EXTRA_LAST_UPDATE_DB_TIME_CATEGORY";
//    public static final String INTENT_EXTRA_THREAD_ID = "INTENT_EXTRA_THREAD_ID";
//    public static final String INTENT_EXTRA_APP_VERSION = "INTENT_EXTRA_APP_VERSION";

    public static final String INTENT_EXTRA_FCM_TOKEN_ID = "INTENT_EXTRA_FCM_TOKEN_ID";
    public static final String INTENT_EXTRA_APP_TOKEN_ID = "INTENT_EXTRA_APP_TOKEN_ID";

    public static final String INTENT_EXTRA_STATUS = "INTENT_EXTRA_STATUS";
    public static final String INTENT_EXTRA_CALL_STATUS = "INTENT_EXTRA_CALL_STATUS";

    public static final String INTENT_EXTRA_USER_NAME = "INTENT_EXTRA_USER_NAME";
    public static final String INTENT_EXTRA_USER_CAR_NUMBER = "INTENT_EXTRA_USER_CAR_NUMBER";
    public static final String INTENT_EXTRA_USER_SEEN_ORDER_ID = "INTENT_EXTRA_USER_SEEN_ORDER_ID";
    public static final String INTENT_EXTRA_USER_SEEN_NOTIFICATION_ID = "INTENT_EXTRA_USER_SEEN_NOTIFICATION_ID";

    public static final String INTENT_EXTRA_ORDER_MODEL = "INTENT_EXTRA_ORDER_MODEL";
    public static final String INTENT_EXTRA_ORDER_MODELS = "INTENT_EXTRA_ORDER_MODELS";
    public static final String INTENT_EXTRA_APP_ORDER_PAGER_POSITION = "INTENT_EXTRA_APP_ORDER_PAGER_POSITION";

    public static final String INTENT_EXTRA_LOCATION_LATITUDE = "INTENT_EXTRA_LOCATION_LATITUDE";
    public static final String INTENT_EXTRA_LOCATION_LONGITUDE = "INTENT_EXTRA_LOCATION_LONGITUDE";

    public static TrayPrefs prefs = new TrayPrefs(ApplicationController.getInstance());

    public static HashMap<String, TrayPrefs> trayPrefs = new HashMap<>();

    public static TrayPrefs getPrefs(String tag) {
        if (!trayPrefs.containsKey(tag)) {
            trayPrefs.put(tag, new TrayPrefs(ApplicationController.getInstance(), tag));
        }
        return trayPrefs.get(tag);
    }

//    public static void saveLastUpdateDBTime(String categoryId) {
//        prefs.putLong(INTENT_EXTRA_LAST_UPDATE_DB_TIME_CATEGORY + categoryId, System.currentTimeMillis());
//    }
//
//    public static long getLastSavedUpdateDbTime(String categoryId) {
//        return prefs.getLong(INTENT_EXTRA_LAST_UPDATE_DB_TIME_CATEGORY + categoryId, 0);
//    }
//
//    public static void clearLastSavedUpdateDbTime(int categoryId) {
//        prefs.remove(INTENT_EXTRA_LAST_UPDATE_DB_TIME_CATEGORY + categoryId);
//    }

    public static void saveFCMToken(String fcmToken) {
        prefs.putString(INTENT_EXTRA_FCM_TOKEN_ID, fcmToken);
    }

    public static String getFCMToken() {
        return prefs.getString(INTENT_EXTRA_APP_TOKEN_ID, "");
    }

    public static void saveAppToken(String appToken) {
        prefs.putString(INTENT_EXTRA_APP_TOKEN_ID, appToken);
    }

    public static String getAppToken() {
        return prefs.getString(INTENT_EXTRA_APP_TOKEN_ID, "");
    }

    public static void removeAppToken() {
        prefs.remove(INTENT_EXTRA_APP_TOKEN_ID);
    }

    public static void saveVehicleStatus(int status) {
        prefs.putInt(INTENT_EXTRA_STATUS, status);
    }

    public static int getVehicleStatus() {
        return prefs.getInt(INTENT_EXTRA_STATUS, 2);
    }

    public static void saveCallStatus(boolean callStatus) {
        prefs.putBoolean(INTENT_EXTRA_CALL_STATUS, callStatus);
    }

    public static boolean getCallStatus() {
        return prefs.getBoolean(INTENT_EXTRA_CALL_STATUS, false);
    }

    public static void saveUserName(String userName) {
        prefs.putString(INTENT_EXTRA_USER_NAME, userName);
    }

    public static String getUserName() {
        return prefs.getString(INTENT_EXTRA_USER_NAME, "");
    }

    public static void removeUserName() {
        prefs.remove(INTENT_EXTRA_USER_NAME);
    }

    public static void saveUserCarNumber(String userCarNumber) {
        prefs.putString(INTENT_EXTRA_USER_CAR_NUMBER, userCarNumber);
    }

    public static String getUserCarNumber() {
        return prefs.getString(INTENT_EXTRA_USER_CAR_NUMBER, "");
    }

    public static void removeUserCarNumber() {
        prefs.remove(INTENT_EXTRA_USER_CAR_NUMBER);
    }

    public static String getSeenOrderIds() {
        return prefs.getString(INTENT_EXTRA_USER_SEEN_ORDER_ID, "");
    }

    public static void saveSeenOrderIds(String seenOrderIds) {
        prefs.putString(INTENT_EXTRA_USER_SEEN_ORDER_ID, seenOrderIds);
    }

    public static String getSeenNotifIds() {
        return prefs.getString(INTENT_EXTRA_USER_SEEN_NOTIFICATION_ID, "");
    }

    public static void saveSeenNotifIds(String seenNotifIds) {
        prefs.putString(INTENT_EXTRA_USER_SEEN_NOTIFICATION_ID, seenNotifIds);
    }

    public static String getLatitude() {
        return prefs.getString(INTENT_EXTRA_LOCATION_LATITUDE, "");
    }

    public static void setLatitude(String latitude) {
        prefs.putString(INTENT_EXTRA_LOCATION_LATITUDE, latitude);
    }

    public static String getLongitude() {
        return prefs.getString(INTENT_EXTRA_LOCATION_LONGITUDE, "");
    }

    public static void setLongitude(String longitude) {
        prefs.putString(INTENT_EXTRA_LOCATION_LONGITUDE, longitude);
    }
}
