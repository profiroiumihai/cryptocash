// Generated code from Butter Knife. Do not modify!
package com.crypto.cash.activities;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextSwitcher;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.crypto.cash.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SplashActivity_ViewBinding implements Unbinder {
  private SplashActivity target;

  @UiThread
  public SplashActivity_ViewBinding(SplashActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public SplashActivity_ViewBinding(SplashActivity target, View source) {
    this.target = target;

    target.logoImageView = Utils.findRequiredViewAsType(source, R.id.splash_iv_logo, "field 'logoImageView'", ImageView.class);
    target.splashContainer = Utils.findRequiredViewAsType(source, R.id.splash_container, "field 'splashContainer'", ViewGroup.class);
    target.firstTextView = Utils.findRequiredViewAsType(source, R.id.first_text_view, "field 'firstTextView'", TextView.class);
    target.secondTextView = Utils.findRequiredViewAsType(source, R.id.second_text_view, "field 'secondTextView'", TextSwitcher.class);
    target.buttonIntro = Utils.findRequiredViewAsType(source, R.id.splash_button_intro, "field 'buttonIntro'", Button.class);
    target.buttonLogin = Utils.findRequiredViewAsType(source, R.id.splash_button_login, "field 'buttonLogin'", Button.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    SplashActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.logoImageView = null;
    target.splashContainer = null;
    target.firstTextView = null;
    target.secondTextView = null;
    target.buttonIntro = null;
    target.buttonLogin = null;
  }
}
